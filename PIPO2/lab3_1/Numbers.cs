﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3_1
{
    public class Numbers
    {
        protected int a;
        protected int b;
        protected int c;

        public Numbers()
        {
            a = 0;
            b = 0;
            c = 0;
        }

        public int GetA() => a;

        public int GetB() => b;

        public int GetC() => c;

        public void SetA(int a)
        {
            this.a = a;
        }
        public void SetB(int b)
        {
            this.b = b;
        }
        public void SetC(int c)
        {
            this.c = c;
        }
    }
}
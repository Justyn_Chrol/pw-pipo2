﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var l1 = new Numbers();
            var l2 = new Numbers();
            l1.SetA(1);
            l1.SetB(2);
            l1.SetC(3);
            Console.WriteLine("L1 a: "+ l1.GetA() +"L1 b: "+ l1.GetB() + "L1 c: " + l1.GetC());
            Console.WriteLine("L2 a: " + l2.GetA() + "L2 b: " + l2.GetB() + "L2 c: " + l2.GetC());
            var ll1 = new BetterNumbers();
            Console.WriteLine("LL1 a: " + ll1.GetA() + "LL1 b: " + ll1.GetB() + "LL1 c: " + ll1.GetC());

        }
    }
}

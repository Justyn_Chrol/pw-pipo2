﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace lab6_1
{
    [TestClass]
    class FactorialUnitTest
    {
        [TestMethod]
        public void TestMethod()
        {
            var factorial = new Factorial();

            Assert.AreEqual(factorial.Calculate(0), 1);
            Assert.AreEqual(factorial.Calculate(1), 1);
            Assert.AreEqual(factorial.Calculate(5), 120);
        }
        }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace lab6_1
{
    class Factorial
    {
        public BigInteger Calculate(BigInteger x)
        {
            BigInteger result = x;
            x--;
            while (x > 1)
            {
                result *= x;
                x--;
            }
            return result;
        }
    }
}

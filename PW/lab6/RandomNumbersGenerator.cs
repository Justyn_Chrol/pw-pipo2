﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab6
{
    public class RandomNumbersGenerator
    {
        private static Random random = new Random();

        public static int GeneratePrimeNumber( int range)
        {
            int primeNumber = 0;
            for(; ; )
            {
                int number = random.Next(range);
                if (numberIsPrime(number))
                    break;
            }
            return primeNumber;
        }

        private static bool numberIsPrime(int number)
        {
            if (number < 2)
                return false; 

            for (int i = 2; i * i <= number; i++)
                if (number % i == 0)
                    return false; 

            return true;
        }
    }
}

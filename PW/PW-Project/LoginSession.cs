﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Imap;
using MailKit.Net.Smtp;
using MailKit.Security;

namespace PW_Project
{
    static class LoginSession
    {
        public static SmtpClient smtp { get; set; }
        public static ImapClient imap { get; set; }
        public static string LoggedUserEmail { get; set; }

        public static bool Login(string emailAddress, string password)
        {
            try
            {
                smtp.Authenticate(emailAddress, password);
                imap.Authenticate(emailAddress, password);
                LoggedUserEmail = emailAddress;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public static void DisconnectWithGmailServer()
        {
            if (imap != null)
            {
                 imap.Disconnect(true);
            }
            if (smtp != null)
            {
                 smtp.Disconnect(true);
            }
        }

        public  static void ConncectWithGmailServer()
        {
             ConnectWithSmtpClient();
             ConnectWithImapClient();
        }
        private static void ConnectWithSmtpClient()
        {
            smtp = new SmtpClient();
            smtp.ServerCertificateValidationCallback = (s, c, h, e) => true;
            smtp.Connect("smtp.gmail.com", 587, SecureSocketOptions.StartTls);
        }

        private static void ConnectWithImapClient()
        {
            imap = new ImapClient();
            imap.ServerCertificateValidationCallback = (s, c, h, e) => true;
            imap.Connect("imap.gmail.com", 993, true);
        }
    }
}

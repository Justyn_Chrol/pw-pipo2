﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PW_Project.Forms
{
    public partial class InboxForm : Form
    {
        private Inbox inbox;

        private string content1;
        private string content2;
        private string content3;

        public InboxForm()
        {
            InitializeComponent();
            CreateList();
            label1.Text = "Inbox " + LoginSession.LoggedUserEmail;

        }
        private void button1_Click(object sender, EventArgs e)
        {
            var view = new EmailContentForm(content1);
            view.Show();
        }
        private void button2_Click_1(object sender, EventArgs e)
        {
            var view = new EmailContentForm(content2);
            view.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var view = new EmailContentForm(content3);
            view.Show();
        }

        private void label1_Click(object sender, EventArgs e)
        {
      
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var menu = new MenuForm();
            menu.Show();
            this.Hide();
        }

        private void RefreshButton_Click(object sender, EventArgs e)
        {
            CreateList();
            this.Refresh();
        }

        private void CreateList()
        {
            inbox = new Inbox();
            var mail = inbox.messages.ElementAt(0);
            button1.Text = mail.Subject;
            var mail1 = inbox.messages.ElementAt(1);
            button2.Text = mail1.Subject;
            var mail2 = inbox.messages.ElementAt(2);
            button3.Text = mail2.Subject;

            content1 = mail.TextBody;
            content2 = mail1.TextBody;
            content3 = mail2.TextBody;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PW_Project;

namespace PW_Project.Forms
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
            label2.Visible = false;
        }

        private void button1_ClickAsync(object sender, EventArgs e)
        {
            var email = textBox1.Text;
            var password = textBox2.Text;
            var loginResult = LoginSession.Login(email,password);
            if (loginResult == true)
            {
                var emailForm = new MenuForm();
                emailForm.Show();
                this.Hide();
            }
            else
            {
                label2.Visible = true;
            }
          
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}

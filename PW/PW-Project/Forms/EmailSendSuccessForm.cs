﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PW_Project.Forms
{
    public partial class EmailSendSuccessForm : Form
    {
        public EmailSendSuccessForm()
        {
            InitializeComponent();
        }

        private void EmailSendSuccessForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var menu = new MenuForm();
            menu.Show();
            this.Hide();
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

﻿using PW_Project.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PW_Project
{
    public partial class EmailSendForm : Form
    {
        public EmailSendForm()
        {
            InitializeComponent();
        }

        private  void button1_Click(object sender, EventArgs e)
        {
            var email = textBox1.Text;
            var subject = textBox2.Text;
            var content = richTextBox1.Text;
            var emailSender = new EmailSender();
            emailSender.SendEmail(email, subject, content);
            var success = new EmailSendSuccessForm();
            success.Show();
            this.Hide();
        }

        private void EmailSendForm_Load(object sender, EventArgs e)
        {
            
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

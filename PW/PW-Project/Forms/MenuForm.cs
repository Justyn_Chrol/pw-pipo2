﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PW_Project.Forms
{
    public partial class MenuForm : Form
    {
        public MenuForm()
        {
            InitializeComponent();
            label1.Text = "Welcome " + LoginSession.LoggedUserEmail;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var emailSend = new EmailSendForm();
            emailSend.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var emailSend = new InboxForm();
            emailSend.Show();
            this.Hide();
        }
        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

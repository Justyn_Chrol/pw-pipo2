﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using System.Threading.Tasks;

namespace PW_Project
{
    class EmailSender
    {

        public void SendEmail(string email,string subject, string content)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("Video App Staff", LoginSession.LoggedUserEmail));
            message.To.Add(new MailboxAddress("New user", email));
            message.Subject = subject;
            message.Body = new TextPart
            {
                Text = content
            };
            LoginSession.smtp.Send(message);
        }
    }
}

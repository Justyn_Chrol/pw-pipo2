﻿using MailKit;
using MailKit.Search;
using System;
using System.Collections.Generic;
using System.Text;
using MimeKit;
namespace PW_Project
{
    class Inbox
    {
        public List<MimeMessage> messages { get; set; }
        public Inbox()
        {
            messages = GetAllMessages();
        }
        private List<MimeMessage> GetAllMessages()
        {
            var messages = new List<MimeMessage>();

            var inbox = LoginSession.imap.Inbox;
            inbox.Open(FolderAccess.ReadOnly);
            var results = inbox.Search(SearchOptions.All, SearchQuery.NotSeen);

            foreach (var uniqueId in results.UniqueIds)
            {
                var message = inbox.GetMessage(uniqueId);
                messages.Add(message);
            }

            messages.Reverse();
            return messages;
        }
    }
}

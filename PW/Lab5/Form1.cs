﻿using Lab5.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using Lab5.Models;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab5
{
    public partial class Form1 : Form
    {
        private AppDbContext _context { get; set; }
        public Form1(AppDbContext dbContext)
        {
            InitializeComponent();
            _context = dbContext;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var property = textBox1.Text;
            _context.Entities.Add(new Entity
            {
                Id = Guid.NewGuid().ToString(),
                Propert = property
            }) ;
            _context.SaveChanges();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            listView1.Clear();
            List<Entity> entities = _context.Entities.ToList();
            foreach (var entity in entities) {
                listView1.Items.Add(entity.Propert);
        }
            Refresh();
        }

    }
}
